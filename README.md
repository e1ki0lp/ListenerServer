# ListenerServer

#### 项目介绍
创建Windows服务，后台监控USB设备     
如果有可移动存储设备插入，直接将可移动存储设备的文件复制到config文件中指定的路径

#### 软件架构
核心文件是[UsbDriverWatcher.cs](ListenerService/UsbDriverWatcher.cs)     
windows服务 不同于 windows用户应用程序，它没有主界面的消息循环     
所以监听USB插入这里使用的是WQL(SQL for WMI)来实现

#### 安装教程

windows服务没有用户窗体，只能在服务列表里面进行启动，停止操作。      
在[Program.cs](ListenerService/Program.cs#L17)中通过命令行参数进行自安装、启动、停止、卸载等操作
##### 安装
xxx --install
##### 卸载
xxx --uninstall
##### 启动
xxx --start
##### 停止
xxx --stop

安装后也可以在"服务"列表中手动启动、停止
