﻿using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;

namespace ListenerService
{
    internal static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        private static void Main(string[] args)
        {
            if (Environment.UserInteractive)
            {
                string param = string.Concat(args);
                ProjectInstaller projectInstaller = new ProjectInstaller();
                string serverName = projectInstaller.serviceInstaller1.ServiceName;
                switch (param)
                {
                    case "--install":
                        ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });	//安装服务
                        break;

                    case "--uninstall":
                        ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location });	//卸载服务
                        break;

                    case "--start":
                        StartService(serverName);
                        break;

                    case "--stop":
                        StopService(serverName);
                        break;

                    default:
                        break;
                }
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new MyService(),
                };
                ServiceBase.Run(ServicesToRun);
            }
        }

        /// <summary>
        /// 启动服务
        /// </summary>
        /// <param name="serverName">服务名称</param>
        /// <param name="timeOut">启动超时，默认30秒</param>
        /// <returns>成功：true，失败：false</returns>
        public static bool StartService(string serverName, int timeOut = 30000)
        {
            ServiceController service = new ServiceController(serverName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeOut);
                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                return true;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                Trace.TraceError(ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// 停止服务
        /// </summary>
        /// <param name="srv_name">服务名称</param>
        /// <param name="time_out">停止超时，默认30秒</param>
        /// <returns>成功：true，失败：false</returns>
        public static bool StopService(string srv_name, int time_out = 30000)
        {
            ServiceController service = new ServiceController(srv_name);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(time_out);
                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                return true;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                Trace.TraceError(ex.StackTrace);
                return false;
            }
        }
    }
}